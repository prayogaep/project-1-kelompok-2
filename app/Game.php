<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'game';

    protected $fillable = ['title','description','image','genre_id'];

    public function genre()
    {
        return $this->belongsTo('\App\Genre');
    }

    public function developer()
    {
        return $this->belongsTo('\App\Developer');
    }

    public function platform()
    {
        return $this->belongsTo('\App\Platform');
    }
}
