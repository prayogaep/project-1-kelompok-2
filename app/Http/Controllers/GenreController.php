<?php

namespace App\Http\Controllers;

use App\Genre;

use RealRashid\SweetAlert\Facades\Alert;


use Illuminate\Http\Request;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Index()
    {
        $genre = Genre::all();
        return view('genre.Index', compact('genre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        return view('genre.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $genre = new Genre;
        $genre->name = $request->name;
        $genre->save();

        Alert::success('Success', 'New Genre has been added!');
        return redirect('/genre');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($genre_id)
    {
        $genre = Genre::where('id', $genre_id)->first();
        return view('genre.detail', compact('genre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($genre_id)
    {
        $genre = Genre::where ('id', $genre_id)->first();
        return view('genre.edit', compact('genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $genre_id)
    {
        $genre = Genre::find($genre_id);
        $request->validate([
            'name' => 'required',
        ]);

        $genre->name = $request->name;
        $genre->save();
        Alert::success('Success', 'Genre has been Updated!');
        return redirect('/genre');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($genre_id)
    {
        $genre = Genre::find($genre_id);
        $genre->delete();
        Alert::success('Success', 'Genre has been deleted!');
        return redirect('/genre');
    }
}
