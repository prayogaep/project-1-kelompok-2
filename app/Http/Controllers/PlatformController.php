<?php

namespace App\Http\Controllers;

use App\Platform;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;

class PlatformController extends Controller
{
    public function Index()
    {
        $platform = Platform::all();
        return view('platform.Index', compact('platform'));
    }

    public function create()
    {
        $platform = Platform::all();
        return view('platform.create', compact('platform'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $platform = new Platform;
        $platform->name = $request->name;
        $platform->save();
        Alert::success('Success', 'New Platform has been added!');
        return redirect('/platform');
    }

    public function show($platform_id)
    {
        $platform = Platform::where('id', $platform_id)->first();
        return view('platform.detail', compact('platform'));
    }

    public function edit($platform_id)
    {
        $platform = Platform::where ('id', $platform_id)->first();
        return view('platform.edit', compact('platform'));
    }

    public function update(Request $request, $platform_id)
    {
        $platform = Platform::find($platform_id);
        $request->validate([
            'name' => 'required',
        ]);

        $platform->name = $request->name;
        $platform->save();
        Alert::success('Success', 'Platform has been updated!');
        return redirect('/platform');
    }

    public function destroy($platform_id)
    {
        $platform = Platform::find($platform_id);
        $platform->delete();
        Alert::success('Success', 'Platform has been deleted!');
        return redirect('/platform');
    }
}
