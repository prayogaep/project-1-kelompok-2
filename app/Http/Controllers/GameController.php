<?php

namespace App\Http\Controllers;

use App\Game;
use App\Genre;
use App\Review;
use App\Platform;
use App\Developer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class GameController extends Controller
{
    public function create()
    {
        $genre = Genre::all();
        $developer = Developer::all();
        $platform = Platform::all();
        return view('game.create', compact('genre','developer','platform'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png|max:2200',
            'genre_id' => 'required',
            'developer_id' => 'required',
            'platform_id' => 'required',
        ]);
        $image = $request->file('image');
        $new_image = time() . '-' . $image->getClientOriginalName();
        $imagePath = public_path('/img/');
        $image->move($imagePath, $new_image);


        $game = new Game;
        $game->title = $request->title;
        $game->description = $request->description;
        $game->image =  $new_image;
        $game->genre_id = $request->genre_id;
        $game->developer_id = $request->developer_id;
        $game->platform_id = $request->platform_id;
        $game->save();

        Alert::success('Success', 'New Game has been added!');

        return redirect('/game');
    }

    public function index()
    {
        $game = Game::all();
        return view('game.index', compact('game'));
    }

    public function post()
    {
        $game = Game::all();
        $genre = Genre::all();
        return view('post.game', compact('game', 'genre'));
    }

    public function show($game_id)
    {
        $comment = Review::where('game_id', $game_id)->get();
        $game = Game::where('id', $game_id)->first();
        
        return view('game.show', compact('game', 'comment'));
    }


    public function edit($game_id)
    {
        $genre = Genre::all();
        $developer = Developer::all();
        $platform = Platform::all();
        $game = Game::where('id', $game_id)->first();
        return view('game.edit', compact('game', 'genre','developer','platform'));
    }

    public function update(Request $request, $game_id)
    {
        $game = Game::find($game_id);
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'genre_id' => 'required',
        ]);

        if ($request->hasFile('image')) {
            unlink(public_path('/img/' . $game->image));
            $image = $request->file('image');
            $new_image = time() . '-' . $image->getClientOriginalName();
            $imagePath = public_path('/img/');
            $image->move($imagePath, $new_image);
            $game->image =  $new_image;
        }

        $game->title = $request->title;
        $game->description = $request->description;
        $game->genre_id = $request->genre_id;
        
        $game->save();
        Alert::success('Success', 'Game has been updated!');
        return redirect('/game');
    }

    public function destroy($game_id)
    {
        $game = Game::find($game_id);
        $destination = 'img/'. $game->image;
        if(File::exists($destination))
        {
            File::delete($destination);
        }
        $game->delete();
        Alert::success('Success', 'Game has been deleted!');
        return redirect('/game');
    }
}
