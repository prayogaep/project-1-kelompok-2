<?php

namespace App\Http\Controllers;

use App\Developer;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;


class DeveloperController extends Controller
{
    public function Index()
    {
        $developer = Developer::all();
        return view('developer.Index', compact('developer'));
    }

    public function create()
    {
        $developer = Developer::all();
        return view('developer.create', compact('developer'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $developer = new Developer;
        $developer->name = $request->name;
        $developer->save();
        return redirect('/developer');
    }

    public function show($developer_id)
    {
        $developer = Developer::where('id', $developer_id)->first();
        return view('developer.detail', compact('developer'));
    }

    public function edit($developer_id)
    {
        $developer = Developer::where ('id', $developer_id)->first();
        return view('developer.edit', compact('developer'));
    }

    public function update(Request $request, $developer_id)
    {
        $developer = Developer::find($developer_id);
        $request->validate([
            'name' => 'required',
        ]);

        $developer->name = $request->name;
        $developer->save();
        return redirect('/developer');
    }

    public function destroy($developer_id)
    {
        $developer = Developer::find($developer_id);
        $developer->delete();
        return redirect('/developer');
    }
}
