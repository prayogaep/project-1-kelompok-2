<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function review(Request $request)
    {
        $validatedData = $request->validate([
            'rating' => 'required',
            'game_id' => 'required',
            'content' => 'required',
        ]);

        $validatedData['user_id'] = Auth::user()->id;

        Review::create($validatedData);
        return redirect('/detail/' . $validatedData['game_id']);

    }
}
