<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Developer extends Model
{
    protected $table = 'developer';

    protected $guarded = ['id'];

    public function platform()
    {
        return $this->belongsTo('\App\Platform', 'developer_id');
    }
}
