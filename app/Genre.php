<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = 'genre';

    protected $guarded = ['id'];
    
    public function game()
    {
        return $this->belongsTo('\App\Game', 'genre_id');
    }
}
