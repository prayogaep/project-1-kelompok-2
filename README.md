# Final Project

## Kelompok 2

### Anggota Kelompok

- *Arnoud Reinhardt Oscar Sarayar*
- *Nurdien Adijaya*
- *Prayoga Erlangga Putra*

## Tema Project

Game Review

## ERD

<img align="center" src="public/assets/ERD.png">

Link Video
===

Link Video Demo Aplikasi : <br>
[![Watch the video](https://i.ytimg.com/an_webp/V2Jk2TVUYVY/mqdefault_6s.webp?du=3000&sqp=CKzwt5EG&rs=AOn4CLBEbcyWZ74cO77Sl9GV_2xx5xpeEg)](https://youtu.be/V2Jk2TVUYVY)
<br>
Link Deploy :<br>
Stuck in the process of deploying to heroku https://gamesreviewcommunity.herokuapp.com/
