<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','IndexController@index');
Route::get('/detail/{game_id}','GameController@show');


Auth::routes();
//midleware
Route::group(['middleware' => ['auth']], function () {

Route::post('/comment', 'ReviewController@review');

// Profile
Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
]);

// CRUD Game
// Create
Route::get('/game/create','GameController@create');
Route::post('/game','GameController@store');
// Read
Route::get('/game','GameController@post');
// Update
Route::get('/game/{game_id}/edit','GameController@edit');
Route::put('/game/{game_id}','GameController@update');
// Delete
Route::delete('/game/{game_id}','GameController@destroy');

// CRUD Genre
// Create
Route::get('/genre/create','GenreController@create'); //show add genre form  page
Route::post('/genre','GenreController@store'); //post from add genre page to database
// Read
Route::get('/genre','GenreController@Index'); //page view in admin
Route::get('/genre/{genre_id}','GenreController@show'); //detail
// Update
Route::get('/genre/{genre_id}/edit','GenreController@edit'); //route to genre edit page
Route::put('/genre/{genre_id}','GenreController@update'); // to post from genre edit page
// Delete
Route::delete('/genre/{genre_id}','GenreController@destroy'); //menghapus record genre


// CRUD Developer
// Create
Route::get('/developer/create','DeveloperController@create');
Route::post('/developer','DeveloperController@store');
// Read
Route::get('/developer','DeveloperController@Index');
Route::get('/developer/{developer_id}','DeveloperController@show');
// Update
Route::get('/developer/{developer_id}/edit','DeveloperController@edit');
Route::put('/developer/{developer_id}','DeveloperController@update');
// Delete
Route::delete('/developer/{developer_id}','DeveloperController@destroy');

// CRUD Platform
// Create
Route::get('/platform/create','PlatformController@create');
Route::post('/platform','PlatformController@store');
// Read
Route::get('/platform','PlatformController@Index');
Route::get('/platform/{platform_id}','PlatformController@show');
// Update
Route::get('/platform/{platform_id}/edit','PlatformController@edit');
Route::put('/platform/{platform_id}','PlatformController@update');
// Delete
Route::delete('/platform/{platform_id}','PlatformController@destroy');
});
