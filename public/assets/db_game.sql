-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Mar 2022 pada 15.06
-- Versi server: 10.4.22-MariaDB
-- Versi PHP: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_game`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `developer`
--

CREATE TABLE `developer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `developer`
--

INSERT INTO `developer` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Konami', '2022-03-12 21:17:53', '2022-03-12 21:17:53'),
(3, 'Namco', '2022-03-12 22:20:07', '2022-03-12 22:20:07'),
(4, 'Bandai', '2022-03-12 22:20:13', '2022-03-12 22:20:13'),
(5, 'Sony Interactive Entertainment', '2022-03-12 22:20:35', '2022-03-12 22:20:35'),
(6, 'Amccus', '2022-03-12 22:20:57', '2022-03-12 22:20:57'),
(7, 'NetherRealm Studios', '2022-03-12 22:25:11', '2022-03-12 22:25:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `game`
--

CREATE TABLE `game` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `developer_id` bigint(20) UNSIGNED NOT NULL,
  `genre_id` bigint(20) UNSIGNED NOT NULL,
  `platform_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `game`
--

INSERT INTO `game` (`id`, `title`, `description`, `image`, `developer_id`, `genre_id`, `platform_id`, `created_at`, `updated_at`) VALUES
(1, 'Bishi Bashi Special', 'this is a game description of Bishi Bashi Special', '1647145734-5cc3b97352feff1a3d55b0fd55aa3cc9.jpg', 2, 1, 1, '2022-03-12 21:28:54', '2022-03-12 21:35:43'),
(2, 'Tekken', 'This is a game description of Tekken', '1647148933-4059235809.jpg', 3, 2, 1, '2022-03-12 22:22:14', '2022-03-12 22:22:14'),
(3, 'Mortal Kombat', 'This is a description of Mortal Kombat', '1647149169-KkYnko4yU5u04spJ.jpg', 7, 4, 1, '2022-03-12 22:26:09', '2022-03-12 22:26:09'),
(4, 'Harvest Moon', 'This is a description of Harvest Moon', '1647149310-capsule_616x353.jpg', 6, 1, 1, '2022-03-12 22:28:30', '2022-03-12 22:28:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `genre`
--

CREATE TABLE `genre` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `genre`
--

INSERT INTO `genre` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'J-rpg', '2022-03-12 21:18:54', '2022-03-12 21:18:54'),
(2, 'Action', '2022-03-12 22:17:35', '2022-03-12 22:17:35'),
(3, 'Adventure', '2022-03-12 22:17:46', '2022-03-12 22:17:46'),
(4, 'RPG', '2022-03-12 22:18:06', '2022-03-12 22:18:06'),
(5, 'Strategy', '2022-03-12 22:18:17', '2022-03-12 22:18:17'),
(6, 'First Person Shooter', '2022-03-12 22:18:30', '2022-03-12 22:18:30'),
(7, 'Arcade', '2022-03-12 22:18:40', '2022-03-12 22:18:40'),
(8, 'Sports', '2022-03-12 22:18:53', '2022-03-12 22:18:53'),
(9, 'Board Game', '2022-03-12 22:19:00', '2022-03-12 22:19:00'),
(10, 'Shooter', '2022-03-13 04:50:05', '2022-03-13 04:50:05'),
(11, 'Simulation', '2022-03-13 04:50:23', '2022-03-13 04:50:23'),
(12, 'Fighting', '2022-03-13 04:50:50', '2022-03-13 04:50:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2022_03_13_040115_create_profile_table', 1),
(5, '2022_03_13_040211_create_developer_table', 1),
(6, '2022_03_13_040308_create_platform_table', 1),
(7, '2022_03_13_040340_create_genre_table', 1),
(8, '2022_03_13_040421_create_game_table', 1),
(9, '2022_03_13_040655_create_review_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `platform`
--

CREATE TABLE `platform` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `platform`
--

INSERT INTO `platform` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Playstation', '2022-03-12 21:17:00', '2022-03-12 21:17:00'),
(2, 'Android', '2022-03-12 22:14:52', '2022-03-12 22:14:52'),
(3, 'Playstation Portable', '2022-03-12 22:15:36', '2022-03-12 22:15:36'),
(4, 'PC', '2022-03-12 22:16:01', '2022-03-12 22:16:01'),
(5, 'Xbox', '2022-03-12 22:16:28', '2022-03-12 22:16:28'),
(6, 'Nintendo', '2022-03-12 22:16:38', '2022-03-12 22:16:38'),
(7, 'Game Boy', '2022-03-12 22:16:56', '2022-03-12 22:16:56'),
(8, 'NES/SNES', '2022-03-12 22:17:17', '2022-03-12 22:17:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profile`
--

CREATE TABLE `profile` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `age` int(11) NOT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `profile`
--

INSERT INTO `profile` (`id`, `age`, `bio`, `country`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 29, 'asd', 'zxc', 1, '2022-03-12 21:16:09', '2022-03-12 21:16:09'),
(2, 29, 'asdzxc', 'qweasdzxc', 2, '2022-03-12 22:10:06', '2022-03-12 22:10:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `review`
--

CREATE TABLE `review` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rating` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `game_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `review`
--

INSERT INTO `review` (`id`, `rating`, `content`, `game_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 9, 'Old But Gold', 1, 1, '2022-03-12 21:45:22', '2022-03-12 21:45:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'nurdienadijaya', 'nurdienadijaya@gmail.com', NULL, '$2y$10$06nFqANLqw9IKYe63HlrXe289js5jbKGzjKoIwN0/iKGbWkGgTLCK', NULL, '2022-03-12 21:16:09', '2022-03-12 21:16:09'),
(2, 'LordNurd', 'nurdien1@dummy.com', NULL, '$2y$10$XFTTITcY1WqPIpxDj8p3wOVtiBmoePoNIdA.rfH9VBUNi2ZWvESui', NULL, '2022-03-12 22:10:06', '2022-03-12 22:10:06');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `developer`
--
ALTER TABLE `developer`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`),
  ADD KEY `game_developer_id_foreign` (`developer_id`),
  ADD KEY `game_genre_id_foreign` (`genre_id`),
  ADD KEY `game_platform_id_foreign` (`platform_id`);

--
-- Indeks untuk tabel `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `platform`
--
ALTER TABLE `platform`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `review_game_id_foreign` (`game_id`),
  ADD KEY `review_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `developer`
--
ALTER TABLE `developer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `game`
--
ALTER TABLE `game`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `genre`
--
ALTER TABLE `genre`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `platform`
--
ALTER TABLE `platform`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `profile`
--
ALTER TABLE `profile`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `review`
--
ALTER TABLE `review`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `game_developer_id_foreign` FOREIGN KEY (`developer_id`) REFERENCES `developer` (`id`),
  ADD CONSTRAINT `game_genre_id_foreign` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`),
  ADD CONSTRAINT `game_platform_id_foreign` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`);

--
-- Ketidakleluasaan untuk tabel `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `profile_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  ADD CONSTRAINT `review_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
