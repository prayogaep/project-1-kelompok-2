@extends('layout.master')

@section('title')
    Profile
@endsection

@push('style')
    <link href="{{ asset('assets/css/register.css') }}" rel="stylesheet">
@endpush

@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success col-lg-8" role="alert">
            {{ session('success') }}
        </div>
    @endif
    <div class="wrapper">
        <div class="logo">
            <img src="https://www.pngitem.com/pimgs/m/41-414785_kids-game-logo-hd-png-download.png" alt="logo">
        </div>
        <div class="text-center mt-4 name"> Profile </div>
        <form class="p-3 mt-3" action="/profile/{{ $profile->id }}" method="post">'
            @method('put')
            @csrf
            <div class="form-field d-flex align-items-center"> <span class="far fa-user"></span>
                <input type="text" name="userName" id="userName" placeholder="Username"
                    value="{{ $profile->user->username }}" readonly>
            </div>
            <div class="form-field d-flex align-items-center"> <span class="fa-solid fa-envelope"></span>
                <input type="text" name="email" id="email" placeholder="Email" value="{{ $profile->user->email }}"
                    readonly>
            </div>
            <div class="form-field d-flex align-items-center"> <span class="fa-solid fa-envelope"></span>
                <input type="number" name="age" id="age" placeholder="Age" value="{{ $profile->age }}">
            </div>
            <div class="form-field d-flex align-items-center"> <span class="fa-solid fa-envelope"></span>
                <textarea type="textarea" name="bio" id="bio" placeholder="Biodata">{{ $profile->bio }}</textarea>
            </div>
            <div class="form-field d-flex align-items-center"> <span class="fa-solid fa-envelope"></span>
                <input type="text" name="country" id="country" placeholder="Country" value="{{ $profile->country }}">
            </div>
            <button class="btn mt-3">Update Profile</button>
        </form>
    </div>
@endsection
