@extends('layout.master')

@section('title')
    Halaman Form
@endsection

@push('style')
    <link href="{{ asset('assets/css/register.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="wrapper">
        <div class="logo">
            <img src="https://www.pngitem.com/pimgs/m/41-414785_kids-game-logo-hd-png-download.png" alt="logo">
        </div>
        <div class="text-center mt-4 name"> Game Review </div>
        <form class="p-3 mt-3" action="{{ route('login') }}" method="post">
            @csrf
            {{-- <div class="form-field d-flex align-items-center"> <span class="far fa-user"></span>
                <input type="text" name="userName" id="userName" placeholder="Username">
            </div> --}}
            <div class="form-field d-flex align-items-center"> <span class="far fa-user"></span>
                <input type="email" name="email" id="email" placeholder="Email">
            </div>
            <div class="form-field d-flex align-items-center"> <span class="fas fa-key"></span>
                <input type="password" name="password" id="pwd" placeholder="Password">
            </div>
            <button class="btn mt-3">Login</button>
        </form>
        <div class="text-center fs-6"> <a href="#">Forget password?</a> or <a href="/register">Sign up</a> </div>
    </div>
@endsection
