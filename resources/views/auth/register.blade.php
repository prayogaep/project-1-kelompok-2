@extends('layout.master')

@section('title')
    Register
@endsection

@push('style')
    <link href="{{ asset('assets/css/register.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="wrapper">
        <div class="logo">
            <img src="https://www.pngitem.com/pimgs/m/41-414785_kids-game-logo-hd-png-download.png" alt="logo">
        </div>
        <div class="text-center mt-4 name"> Game Review </div>
        <form class="p-3 mt-3" action="{{ route('register') }}" method="post">
            @csrf

            {{-- input name --}}
            <div class="form-field d-flex align-items-center"> <span class="far fa-user"></span>
                <input type="text" name="username" id="name" placeholder="Username">
            </div>
            @error('username')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            {{-- input password --}}
            <div class="form-field d-flex align-items-center"> <span class="fas fa-key"></span>
                <input type="password" name="password" id="pwd" placeholder="Password">
            </div>
            @error('password')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            {{-- input password confirmation --}}
            <div class="form-field d-flex align-items-center"> <span class="fas fa-key"></span>
                <input type="password" name="password_confirmation" id="pwd" placeholder="Re-type Password">
            </div>

            {{-- input email --}}
            <div class="form-field d-flex align-items-center"> <span class="fa-solid fa-envelope"></span>
                <input type="text" name="email" id="email" placeholder="Email">
            </div>
            @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            {{-- input age --}}
            <div class="form-field d-flex align-items-center"> <span class="fa-solid fa-envelope"></span>
                <input type="number" name="age" id="age" placeholder="Age">
            </div>
            @error('age')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            {{-- input bio --}}
            <div class="form-field d-flex align-items-center"> <span class="fa-solid fa-envelope"></span>
                <textarea type="textarea" name="bio" id="bio" placeholder="Biodata"></textarea>
            </div>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            {{-- input country --}}
            <div class="form-field d-flex align-items-center"> <span class="fa-solid fa-envelope"></span>
                <input type="text" name="country" id="country" placeholder="Country">
            </div>
            @error('country')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <button class="btn mt-3">Register</button>
        </form>
        <div class="text-center fs-6">Already have account? <a href="/login">Login</a> </div>
    </div>
@endsection
