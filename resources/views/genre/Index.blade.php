@extends('layout.master')

@push('style')
    <link href="{{ asset('assets/css/genre-index.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-lg">
                <h1>List Genre</h1>
                @auth
                    <a href="/genre/create" class="btn btn-primary mb-2">Add new Genre</a>
                @endauth
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama</th>
                            @auth
                                <th scope="col">Action</th>
                            @endauth
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($genre as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->name }}</td>
                                @auth
                                    <td>
                                        <a href="/genre/{{ $item->id }}/edit" class="btn btn-warning">Edit</a>
                                        <form action="/genre/{{ $item->id }}" method="post" class="d-inline">
                                            @method('delete')
                                            @csrf
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                @endauth
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No Records Found</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
