@extends('layout.master')

@section('content')
    <div class="container mt-2">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <h1>Create Developer</h1>
                <form action="/developer" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="title">Developer Name</label>
                        <input type="text" class="form-control" id="name" name="name"
                            placeholder="Enter developer name...">
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
