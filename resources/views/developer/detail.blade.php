@extends('layout.master')

@section('content')
    <div class="container mt-2">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <h1>Create Developer</h1>

                <div class="form-group">
                    <label for="title">Developer Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $developer->name }}">
                </div>
                <a href="/developer" class="btn btn-primary">Go back</a>

            </div>
        </div>
    </div>
@endsection
