@extends('layout.master')

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-lg">
                <h1>List Game</h1>
                <a href="/game/create" class="btn btn-primary mb-2">Add new game</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Description</th>
                            <th scope="col">Image</th>
                            <th scope="col">Genre</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($game as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->description }}</td>
                                <td><img src="{{ asset('img/' . $item->image) }}" width="200px" height="200px"
                                        style="object-fit: cover"></td>
                                <td>{{ $item->genre->name }}</td>
                                <td>
                                    <a href="/detail/{{ $item->id }}" class="btn btn-info m-2">Detail</a>
                                    <a href="/game/{{ $item->id }}/edit" class="btn btn-warning m-2">Edit</a>
                                    <form action="/game/{{ $item->id }}" method="post" class="d-inline">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger m-2">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No Records Found</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
