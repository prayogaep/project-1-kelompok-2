@extends('layout.master')

@push('style')
    <link href="{{ asset('assets/css/game-show.css') }}" rel="stylesheet">
    <style>
        .game-detail {
            background-image: linear-gradient(transparent, #ecf0f3),
                url({{ asset('img/' . $game->image) }});
        }

    </style>
@endpush

@section('content')
    <div>
        <div class="game-detail">
            <div class="detail-container">
                <div class="content-container">
                    <div class="top-container">
                        <img class="detail-img my-auto" src="{{ asset('img/' . $game->image) }}" alt="game picture">
                        <div class="pt-4">
                            <h1>{{ $game->title }}</h1>
                            <div class="sub-container">
                                <h6>Platforms</h6>
                                <div class="platform-container">
                                    {{-- <Link to={`/platform/${item.platform.id}`}> --}}
                                    <a href="#" class="platform">{{ $game->platform->name }}
                                    </a>
                                </div>
                            </div>
                            <div class="sub-container">
                                <h6>Genres</h6>
                                <div class="platform-container">
                                    {{-- <Link to={`/genre/${item.slug}`}> --}}
                                    <a href="#" class="platform">{{ $game->genre->name }}
                                    </a>
                                    {{-- </Link> --}}
                                </div>
                            </div>
                            <div class="sub-container">
                                <h6>Developer</h6>
                                <div class="platform-container">
                                    {{-- <Link to={`/genre/${item.slug}`}> --}}
                                    <a href="#" class="platform">{{ $game->developer->name }}
                                    </a>
                                    {{-- </Link> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bottom-container sub-container">
                        <h6>Description</h6>
                        <p>{{ $game->description }}</p>
                    </div>
                </div>
            </div>

            <div class="comments-container">
                <form action="/comment" method="post">
                    @csrf
                    <h1>Comments</h1>
                    <div class="add-comment">
                        <h3>Add Comment</h3>
                        <input class="form-field" type="number" name="rating" id="rating" placeholder="Rating" min="0"
                            max="10">
                        <input class="form-field" type="hidden" value="{{ $game->id }}" name="game_id" id="game_id"
                            placeholder="Rating" min="0" max="10">
                        <textarea class="form-field" placeholder="Your Comment" name="content"></textarea>
                        <button class="btn mt-3" type="submit">Send</button>
                    </div>
                </form>
                <h1>Review From User</h1>
                <div class="comment-flex">
                    @foreach ($comment as $item)
                        <div class="comment-card">
                            <img class=""
                                src="https://ui-avatars.com/api/?background=random&name={{ $item->user->username }}&rounded=true"
                                alt="avatar">
                            <div class="comment-section">
                                <div class="comment-section-top">
                                    <h6>{{ $item->user->username }}</h6>
                                    <h6>{{ $item->rating }}/10</h6>
                                </div>
                                <p>{{ $item->content }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
