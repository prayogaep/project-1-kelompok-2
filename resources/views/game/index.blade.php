@extends('layout.master')

@push('style')
    <link href="{{ asset('assets/css/game-index.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="body">
        <div class="container">
            <div class="title">
                <h1>Find Our Latest Game Review</h1>
                <h2>and join the largest review community</h2>
            </div>
            <div class="game">
                @foreach ($game as $item)
                    <a class="card-container" href="/detail/{{ $item->id }}" style="text-decoration: none; color:black">
                        <div class="cardContainer">
                            <img src="{{ asset('img/' . $item->image) }}" alt="GTAV-poster-game">
                            <h3>{{ $item->title }}</h3>
                            <div class="card-title">
                                <p class="genre">{{ $item->genre->name }}</p>
                                <p class="platform">{{ $item->platform->name }}</p>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection


