@extends('layout.master')

@section('content')
    <div class="container mt-2">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <h1>Create Game</h1>
                <form action="/game" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="title">Game Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter game title...">
                    </div>
                    <div class="form-group">
                        <label for="description">Game Description</label>
                        <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="image">Upload Image</label>
                        <input type="file" class="form-control-file" id="image" name="image">
                    </div>
                    <div class="form-group">
                        <label for="genre_id">Select Genre</label>
                        <select class="form-control js-example-basic-single" id="genre_id" name="genre_id">
                            <option value="" disabled selected> -- Choose Genre --</option>
                            @foreach ($genre as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="platform_id">Select Platform</label>
                        <select class="form-control js-example-basic-single" id="platform_id" name="platform_id">
                            <option value="" disabled selected> -- Choose Platform --</option>
                            @foreach ($platform as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="developer_id">Select Developer</label>
                        <select class="form-control js-example-basic-single" id="developer_id" name="developer_id">
                            <option value="" disabled selected> -- Choose Developer --</option>
                            @foreach ($developer as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.tiny.cloud/1/g1bobzbi96imv0u0fopzkbbugd6p5vnxalgo06mcnjjo39go/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush