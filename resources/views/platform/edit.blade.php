@extends('layout.master')

@section('content')
    <div class="container mt-2">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <h1>Edit Platform</h1>
                <form action="/platform/{{ $platform->id }}" method="post">
                    @method('put')
                    @csrf
                    <div class="form-group">
                        <label for="title">Platform Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ $platform->name }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
