@extends('layout.master')

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-lg">
                <h1>List Platform</h1>
                @auth
                    <a href="/platform/create" class="btn btn-primary mb-2">Add new Platform</a>
                @endauth
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama</th>
                            @auth
                                <th scope="col">Action</th>
                            @endauth
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($platform as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->name }}</td>
                                @auth
                                    <td>
                                        <a href="/platform/{{ $item->id }}/edit" class="btn btn-warning">Edit</a>
                                        <form action="/platform/{{ $item->id }}" method="post" class="d-inline">
                                            @method('delete')
                                            @csrf
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                @endauth
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No Records Found</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
